import CartIconForHeader from "./BtnsForHeader/CartBtnForHeader/Cart";
import FavouriteIconForHeader from "./BtnsForHeader/FavouriteBtnForHeader/FavoriteBtn";
import "./Header.scss";

function Header() {
  return (
    <div className="header">
      <div className="header__icons">
        <CartIconForHeader />
        <FavouriteIconForHeader />
      </div>
    </div>
  );
}
export default Header;

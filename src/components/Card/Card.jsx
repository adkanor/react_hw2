import "./Card.scss";
import FavoriteIcon from "./Favorites-svg-icon/Favorite";
import PropTypes from "prop-types";
import Button from "../Buttons/Button";

const Card = ({ image, name, price, vendorCode }) => {
  return (
    <div className="card">
      <li className="card__item">
        <div className="card__image">
          <img className="card__image" src={image}></img>
        </div>
        <p className="card__name"> {name} </p>
        <p className="card__vendorCode">Сode: {vendorCode}</p>
        <p className="card__price">
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
            currencyDisplay: "narrowSymbol",
          }).format(price)}
        </p>
        <div className="card__action-btns">
          <Button backgroundColor="rgb(116, 34, 50)" text="Add to cart" />
          <FavoriteIcon />
        </div>
      </li>
    </div>
  );
};

Card.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  vendorCode: PropTypes.number.isRequired,
};
export default Card;

import { useContext, useState } from "react";
import "./Favorite.scss";
import { AddOrRemoveToFavouritesBtnFunc } from "../../../App";

const FavoriteIcon = () => {
  const [isSelectedIcon, setIconSelected] = useState(false);
  const { AddToFavourites, RemoveFromFavourites } = useContext(
    AddOrRemoveToFavouritesBtnFunc
  );

  return (
    <svg
      onClick={() => {
        setIconSelected(!isSelectedIcon);
        if (!isSelectedIcon) {
          AddToFavourites();
        } else {
          RemoveFromFavourites();
        }
      }}
      className={isSelectedIcon ? "svg selected" : "svg not-selected"}
      width="40"
      height="40"
      viewBox="0 0 24 24"
      fill="none"
      stroke="white"
    >
      <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
    </svg>
  );
};

export default FavoriteIcon;

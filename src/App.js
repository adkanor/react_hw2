import React from "react";
import { useState, useEffect } from "react";
import Modal from "./components/Modals/Modal";
import FullScreenWrapper from "./components/wrapper/FullScreenWrapper";
import MainSection from "./components/MainSection/MainSection";
import axios from "axios";
import Header from "./components/Header/Header";

export const ItemsSelectedInHeader = React.createContext();
export const AddToCartBtnFunc = React.createContext();
export const AddOrRemoveToFavouritesBtnFunc = React.createContext();

function App() {
  const [amountOfProducts, setProductsToTheCart] = useState(
    Number(localStorage.getItem("amountOfProducts") || 0)
  );
  const [amountOfFavourites, setToFavourites] = useState(
    Number(localStorage.getItem("amountOfFavourites") || 0)
  );
  const [modal, setModal] = useState(false);
  const [closeBtnForModal, setCloseBtnForModal] = useState(true);
  const [products, setProducts] = useState([]);

  const addProductToTheCart = () => {
    setProductsToTheCart((prev) => prev + 1);
  };

  const closeModalWindow = () => {
    setModal(false);
  };
  const AddToFavourites = () => {
    setToFavourites((prev) => prev + 1);
  };
  const RemoveFromFavourites = () => {
    setToFavourites((prev) => prev - 1);
  };

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios
        .get(`${document.location.href}products-data.json`)
        .catch((err) => {
          console.warn(err);
          alert("Something went wrong. Try later.");
        });
      setProducts(response.data);
    };
    fetchData();
  }, []);

  useEffect(() => {
    localStorage.setItem("amountOfProducts", amountOfProducts);
  }, [amountOfProducts]);

  useEffect(() => {
    localStorage.setItem("amountOfFavourites", amountOfFavourites);
  }, [amountOfFavourites]);
  return (
    <>
      <FullScreenWrapper>
        <Modal
          active={modal}
          closeModal={closeModalWindow}
          header="Add this item to the cart?"
          textContent="Are you sure you want to add this item to the card? "
          shouldClose={closeBtnForModal}
          closeModalWindow={closeModalWindow}
          addProductToTheCart={addProductToTheCart}
        />

        <ItemsSelectedInHeader.Provider
          value={{ amountOfProducts, amountOfFavourites }}
        >
          <Header />
        </ItemsSelectedInHeader.Provider>

        <AddOrRemoveToFavouritesBtnFunc.Provider
          value={{ AddToFavourites, RemoveFromFavourites }}
        >
          <AddToCartBtnFunc.Provider
            value={() => {
              setModal(true);
            }}
          >
            <MainSection products={products} />
          </AddToCartBtnFunc.Provider>
        </AddOrRemoveToFavouritesBtnFunc.Provider>
      </FullScreenWrapper>
    </>
  );
}

export default App;
